Script para ejecutar un proxy de nginx en un contenedor de Docker. El siguiente repo es utilizado en un Droplets de [Digital Ocean](https://m.do.co/c/21bb09a93cba) recien creado.

## Definición de las variables de entorno requeridas:
- **DB_HOST=** Nombre del servicio que ejecutara el contenedor de mysql

- **DEFAULT_NETWORK=** Alias de la red a la que se deben conectar el resto de servicios que serán administrados por el proxy

- **DB_PORT=** Puerto de conexión a la DB por defecto es el 3306

- **DB_USER=** Usuario de la database

- **DB_NAME=** Nombre de la database

- **DB_PASSWORD=** contraseña de la database para el usuario creado "$DB_USER"

- **MYSQL_ROOT_PASSWORD=** contraseña de la database para el usuario root


## Pasos de instalación/ejecución:

- Conectarse al servidor mediante `ssh`

- Si es primera vez que se conecta al servidor realizar las actualizaciones pertinentes

```
sudo apt update && sudo apt upgrade -y
```

- Instalar Docker. ver la documentación oficial [aquí](https://docs.docker.com/engine/install/)

- Instalar Docker Composer. ver la documentación oficial [aquí](https://docs.docker.com/compose/install/)

- clonar el repositorio

```
git clone https://gitlab.com/gagzu/docker-proxy
cd docker-proxy
```
- Hacer una copia del fichero de configuración `.env.example` y renombrarlo a `.env`
  > NOTA: en caso de que esté en producción no use la configuración por defecto del fichero `.env.example` se recomienda encarecidamente hacer las modificaciones en las variables de entornos listadas en dicho fichero

- Crear la red de docker a la que se conectara el proxy y el resto de servicios. Recuerda que debe tener el mismo nombre que definimos en el `.env` en la variable `$DEFAULT_NETWORK` que en fichero de ejemplo sería `gagzu-network`

```
docker network create gagzu-network
```
- Ahora ya estamos listos para ejecutar el proxy

```
docker-compose up -d
```

- Listo ya tenemos andando nuestro proxy podemos ingresar a traves de http://\<hostname-o-IP\>:8000 las credenciales por defecto serían:
```
  Email:    admin@example.com
  Password: changeme
```

### Links de referencia

- [Página oficial de NPM Nginx Proxy Manager](https://nginxproxymanager.com/)

**Feliz codificación** ✌💻